package com.epam.maksymenko.hw3

class HW3 {
  def compose[A, B, C](f1: B => C)(f2: A => B): A => C = {
    a: A => f1(f2(a))
  }

  def myWhile(cond: => Boolean)(body: => Unit): Unit = {
    if (cond) {
      body
      myWhile(cond)(body)
    }
  }

  def toList = (e: Seq[Circuit]) => e.toList

  def computeResistance(circuit: Circuit): Double = circuit match {
    case Parallel(args@_*) => 1 / (computeParallel(toList(args)))
    case Series(args@_*) => computeSeries(toList(args))
  }

  private def computeParallel(parallel: List[Circuit]): Double = parallel match {
    case Nil => 0
    case head :: tail => head match {
      case Resistor(v) => 1 / v + computeParallel(tail)
      case Parallel(args@_*) => computeParallel(toList(args)) + computeParallel(tail)
      case Series(args@_*) => 1 / (computeSeries(toList(args))) + computeParallel(tail)
    }
  }

  private def computeSeries(parallel: List[Circuit]): Double = parallel match {
    case Nil => 0
    case head :: tail => head match {
      case Resistor(v) => v + computeSeries(tail)
      case Parallel(args@_*) => (1 / (computeParallel(toList(args)))) + computeSeries(tail)
      case Series(args@_*) => computeSeries(toList(args)) + computeSeries(tail)
    }
  }
}

abstract class Circuit

case class Resistor(resistance: Double) extends Circuit

case class Parallel(e: Circuit*) extends Circuit

case class Series(e: Circuit*) extends Circuit


object Main {
  def main(args: Array[String]): Unit = {
    val f = new HW3().compose((x: String) => x * 3)((x: Int) => (x + 1).toString)
    println(f(1))

    var count = 0
    new HW3().myWhile(count < 5) {
      println("$count")
      count += 1
    }

    val r1: Resistor = Resistor(10)
    val r2: Resistor = Resistor(30)
    val r3: Resistor = Resistor(105)
    val r4: Resistor = Resistor(20)
    val r5: Resistor = Resistor(40)
    val r6: Resistor = Resistor(15)

    println(new HW3().computeResistance(Series(r1, r2)))
    println(new HW3().computeResistance(Parallel(r1, r2)))
    println(new HW3().computeResistance(Parallel(r1, r2, r3)))
    println(new HW3().computeResistance(Series(Parallel(r1, r2), r3)))
    println(new HW3().computeResistance(Series(r4, r5)))
    println(new HW3().computeResistance(Parallel(Series(Parallel(r1, r2), r3), Series(r4, r5))))
    println(new HW3().computeResistance(
      Series(
        Parallel(
          Series(
            Parallel(r1, r2), r3
          ),
          Series(r4, r5)
        ),
        r6
      )
    )
    )
  }
}
