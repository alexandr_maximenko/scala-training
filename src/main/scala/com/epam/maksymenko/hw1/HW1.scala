package com.epam.maksymenko.hw1

import scala.annotation.tailrec

class HW1 {
  def digitsSum(num: Int): Int = {
    if (num > 10) num % 10 + digitsSum(num / 10) else num
  }

  def digitsQuantity(num: Int): Int = {
    if (num > 10) 1 + digitsQuantity(num / 10) else 1;
  }

  def factorialDigitsSum(num: Int): Int = {
    return digitsSum(factorial(num))
  }

  def reverseString(str: String): String = str match {
    case "" => ""
    case _ => str.charAt(str.length - 1) + reverseString(str.substring(0, str.length - 1))
  }

  @tailrec
  final def isPalindrome(str: String): Boolean = {
    if (str.length < 2) {
      true
    } else {
      if (str.charAt(0).toLower == str.charAt(str.length - 1).toLower) {
        isPalindrome(str.substring(1, str.length - 1))
      } else {
        false
      }
    }
  }

  def fib(n: Long): Long = {
    def fibInternal(n: Long, step: Long): Long = {
      if (isFibonacci(n - step) || isFibonacci(n + step)) {
        step
      } else {
        fibInternal(n, step + 1)
      }
    }

    if (isFibonacci(n)) 0 else fibInternal(n, 1)
  }

  def isFibonacci(n: Long): Boolean = n match {
    case 0 => true
    case 1 => true
    case _ => {
      def isFibonacciInternal(num: Long, prev: Long, next: Long): Boolean = {
        if (num == prev + next) {
          true
        } else if (num > (prev + next)) {
          isFibonacciInternal(num, next, prev + next)
        } else {
          false
        }
      }

      isFibonacciInternal(n, 0, 1)
    }
  }

  def isFibonacciPerfectSquare(n: Long): Boolean = {
    def isPerfectSquare(num: Long): Boolean = {
      val sqrt = math.sqrt(num).toInt
      sqrt * sqrt == num
    }

    isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4)
  }

  private def factorial(num: Int): Int = {
    if (num > 1) num * factorial(num - 1) else 1;
  }
}
