package com.epam.maksymenko.hw2

class HW2 {
  def longestStr(strings: List[String]): (String, Int) = {
    def longest(elements: List[String], max: (String, Int)): (String, Int) = {
      elements match {
        case Nil => max
        case head :: tail => {
          val newMax = if (max._2 < head.length) (head, head.length) else max
          longest(tail, newMax)
        }
      }
    }

    longest(strings, ("", 0))
  }

  def evaluate(e: Expr, vals: Map[String, Double]): Double = e match {
    case Sum(l, r) => evaluate(l, vals) + evaluate(r, vals)
    case Minus(l, r) => evaluate(l, vals) - evaluate(r, vals)
    case Multiply(l, r) => evaluate(l, vals) * evaluate(r, vals)
    case Divide(l, r) => evaluate(l, vals) / evaluate(r, vals)
    case Const(const) => const
    case Var(variable) => vals(variable.toString)
  }
}
