package com.epam.maksymenko.hw2

abstract class Expr

case class Sum(exprLeft: Expr, exprRight: Expr) extends Expr

case class Minus(exprLeft: Expr, exprRight: Expr) extends Expr

case class Multiply(exprLeft: Expr, exprRight: Expr) extends Expr

case class Divide(exprLeft: Expr, exprRight: Expr) extends Expr

case class Const(const: Double) extends Expr

case class Var(variable: Char) extends Expr