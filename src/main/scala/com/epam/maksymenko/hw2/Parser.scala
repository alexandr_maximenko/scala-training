package com.epam.maksymenko.homework.hw2

import com.epam.maksymenko.hw2._

import scala.collection.mutable

//TODO Must be refactored!
class Parser {
  val numberRegex = "[0-9]".r
  val variableRegex = "[a-z]".r
  val operations = mutable.Stack[Char]()
  val expression = mutable.Stack[Expr]()
  var stackSize: Int = 0
  var stopOnOpenBracket = false

  type Priority = Char => Int

  def priority: Priority = {
    case '+' | '-' => 1
    case '*' | '/' => 2
    case '(' | ')' => 3
  }

  def expr(operation: Char, l: Expr, r: Expr): Expr = operation match {
    case '+' => Sum(l, r)
    case '-' => Minus(l, r)
    case '*' => Multiply(l, r)
    case '/' => Divide(l, r)
  }


  def parse(str: String): Expr = {
    var string = str
    if (str.startsWith("-")) {
      string = "0" + str
    }
    parseChars(string.toList)
  }

  private def parseChars(chars: List[Char]): Expr = chars match {
    case Nil => {
      while (!operations.isEmpty) {
        convertOperationToExpression()
      }
      expression.pop()
    }
    case head :: tail => {
      parseChar(head)
      parseChars(tail)
    }
  }

  private def parseChar(char: Char) = char match {
    case '+' | '-' | '*' | '/' => {
      if (operations.isEmpty)
        operations.push(char)
      else if (stopOnOpenBracket) {
        operations.push(char)
        stopOnOpenBracket = false
      } else processPriority(char)
    }
    case numberRegex() => expression.push(Const(char.toString.toDouble))
    case variableRegex() => expression.push(Var(char))
    case '(' => stackSize = operations.size; stopOnOpenBracket = true
    case ')' => popUntilOpenBracket(); stopOnOpenBracket = false
    case _ => None
  }

  private def processPriority(operation: Char): Unit = {
    if (stackSize > 0) {
      while (priority(operations.top) == priority(operation)) {
        convertOperationToExpression()
      }
    } else {

      while ((!operations.isEmpty) && (priority(operations.top) >= priority(operation))) {
        convertOperationToExpression()
      }
    }
    operations.push(operation)
  }

  private def convertOperationToExpression(): Unit = {
    val rightExp: Expr = expression.pop
    val leftExp: Expr = expression.pop

    expression.push(expr(operations.pop, leftExp, rightExp))
  }

  private def popUntilOpenBracket(): Unit = {
    for (i <- 1 to operations.size - stackSize) {
      convertOperationToExpression()
    }
    stackSize = 0
  }
}
