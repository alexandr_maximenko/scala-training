package com.epam.maksymenko.hw2

import com.epam.maksymenko.homework.hw2.Parser
import org.junit.Assert._
import org.junit.Test

class HW2Test {
  private val hw2 = new HW2()
  private val parser = new Parser

  @Test
  def shouldGetLongestString(): Unit = {
    assertEquals(("mama myla ramu", 14), hw2.longestStr(List("a", "abcd", "df", "asfasdf", "d", "mama myla ramu", "testovi4")))
  }

  @Test
  def shouldGetEmptyTuple() = {
    assertEquals(("", 0), hw2.longestStr(List()))
  }

  @Test
  def shouldGetExpressionWithoutParamsEvaluated() = {
    val expr = parser.parse("8 * 2 - 4 * 3")
    assertEquals(4, hw2.evaluate(expr, null), 0)
  }

  @Test
  def shouldGetExpressionWithBracketsEvaluated(): Unit = {
    val expr = parser.parse("(4+5)/(4-1)")
    assertEquals(3, hw2.evaluate(expr, null), 0)
  }

  @Test
  def shouldGetExpressionWithManyBracketsEvaluated() = {
    var expr = parser.parse("-1 + (4+1)*1 + (5-4)/(4-1-2)")
    assertEquals(5, hw2.evaluate(expr, null), 0)
  }

  @Test
  def shouldGetExpressionWithBracketsAndParamsEvaluated() = {
    val expr = parser.parse("-1 + (x+1)*y + (z-4)/(x-y-2)")
    assertEquals(5, hw2.evaluate(expr, Map("x" -> 4, "z" -> 5, "y" -> 1)), 0)
  }
}
