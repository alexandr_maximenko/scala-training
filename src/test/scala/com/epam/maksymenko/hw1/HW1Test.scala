package com.epam.maksymenko.hw1

import org.junit.Assert._
import org.junit.Test

class HW1Test {
  private val hw1 = new HW1()

  @Test
  def shouldGetSumOfDigitsOfTwoNumbers() {
    assertEquals(3, hw1.digitsSum(12));
  }

  @Test
  def shouldGetSumOfDigitsOfThreeNumbers() {
    assertEquals(7, hw1.digitsSum(412))
  }

  @Test
  def shouldGetQuantityOfDigitsOfNumber() {
    assertEquals(4, hw1.digitsQuantity(3048))
  }

  @Test
  def shouldGetSumOfDigitsOfFactorialOfNumber() {
    assertEquals(27, hw1.factorialDigitsSum(10))
  }

  @Test
  def shouldGetReversedString() {
    assertEquals("abcd", hw1.reverseString("dcba"))
  }

  @Test
  def shouldGetEmptyStringAfterReversingAnEmptyString(): Unit = {
    assertEquals("", hw1.reverseString(""))
  }

  @Test
  def shouldGetTrueAfterCheckingPalindromeStr(): Unit = {
    assertTrue(hw1.isPalindrome("abcba"))
  }

  @Test
  def shouldGetFalseAfterCheckingNotPalindromeStr(): Unit = {
    assertFalse(hw1.isPalindrome("abcda"))
  }

  @Test
  def shouldGetTrueAfterCheckingPalindromeStrWithDifferentCharCases: Unit = {
    assertTrue(hw1.isPalindrome("AbcdEDCba"))
  }

  @Test
  def shouldGetClosestFibonacciNumber(): Unit = {
    assertEquals(0, hw1.fib(0))
    assertEquals(0, hw1.fib(1))
    assertEquals(0, hw1.fib(2))
    assertEquals(0, hw1.fib(3))
    assertEquals(1, hw1.fib(4))
    assertEquals(3, hw1.fib(16))
    assertEquals(2, hw1.fib(19))
  }

  @Test
  def checkIfGivenNumberIsFibonacciNumber(): Unit = {
    assertTrue(hw1.isFibonacci(0))
    assertTrue(hw1.isFibonacci(1))
    assertTrue(hw1.isFibonacci(2))
    assertTrue(hw1.isFibonacci(3))
    assertFalse(hw1.isFibonacci(4))
    assertTrue(hw1.isFibonacci(5))
    assertFalse(hw1.isFibonacci(6))
    assertFalse(hw1.isFibonacci(7))
    assertTrue(hw1.isFibonacci(8))
    assertFalse(hw1.isFibonacci(9))
    assertFalse(hw1.isFibonacci(10))
    assertFalse(hw1.isFibonacci(11))
    assertFalse(hw1.isFibonacci(12))
    assertTrue(hw1.isFibonacci(13))
    assertFalse(hw1.isFibonacci(14))
    assertFalse(hw1.isFibonacci(15))
    assertFalse(hw1.isFibonacci(16))
    assertFalse(hw1.isFibonacci(17))
    assertFalse(hw1.isFibonacci(18))
    assertFalse(hw1.isFibonacci(19))
    assertFalse(hw1.isFibonacci(20))
    assertTrue(hw1.isFibonacci(21))
  }

  @Test
  def checkIfGivenNumberIsFibonacciNumberWithPerfectSquare(): Unit = {
    assertTrue(hw1.isFibonacciPerfectSquare(0))
    assertTrue(hw1.isFibonacciPerfectSquare(1))
    assertTrue(hw1.isFibonacciPerfectSquare(2))
    assertTrue(hw1.isFibonacciPerfectSquare(3))
    assertFalse(hw1.isFibonacciPerfectSquare(4))
    assertTrue(hw1.isFibonacciPerfectSquare(5))
    assertFalse(hw1.isFibonacciPerfectSquare(6))
    assertFalse(hw1.isFibonacciPerfectSquare(7))
    assertTrue(hw1.isFibonacciPerfectSquare(8))
    assertFalse(hw1.isFibonacciPerfectSquare(9))
    assertFalse(hw1.isFibonacciPerfectSquare(10))
    assertFalse(hw1.isFibonacciPerfectSquare(11))
    assertFalse(hw1.isFibonacciPerfectSquare(12))
    assertTrue(hw1.isFibonacciPerfectSquare(13))
    assertFalse(hw1.isFibonacciPerfectSquare(14))
    assertFalse(hw1.isFibonacciPerfectSquare(15))
    assertFalse(hw1.isFibonacciPerfectSquare(16))
    assertFalse(hw1.isFibonacciPerfectSquare(17))
    assertFalse(hw1.isFibonacciPerfectSquare(18))
    assertFalse(hw1.isFibonacciPerfectSquare(19))
    assertFalse(hw1.isFibonacciPerfectSquare(20))
    assertTrue(hw1.isFibonacciPerfectSquare(21))
  }
}
